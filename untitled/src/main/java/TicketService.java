package com.company;

import com.company.dataaccess.Repository;
import com.company.models.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class TicketService {

    private Repository<Ticket, Integer> ticketRepository;
    private Logger logger = LogManager.getLogger(TicketService.class.getName());


    // The TicketRepository dependency will be injected to the service.
    // Because the service shouldn't have to be concerned with how the Repository is created.
    public TicketService(Repository<Ticket, Integer> ticketRepository) {
        this.ticketRepository = ticketRepository;
    }


    /***
     *<p>This method will validate and save support tickets</p>
     * @param t the ticket to save
     * @return the ticket id
     * @author August Duet
     * @throws nothing
     */
    public Integer saveTicket(Ticket t) {
        logger.debug("Saving ticket {}", t);
        return ticketRepository.save(t);
    }

    public Ticket getTicket(int id) {
        return ticketRepository.getById(id);
    }

    public List<Ticket> getAllTickets() {
        return (List<Ticket>)ticketRepository.getAll();
    }
}
