package com.company;

import Connections.DBConnector;
import Connections.PostgreSQLDBConnector;
import com.company.dataaccess.Repository;
import com.company.dataaccess.TicketRepository;
import com.company.dataaccess.TicketSQLRepository;
import com.company.models.ProductSupportTicket;
import com.company.models.TechSupportTicket;
import com.company.models.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Scanner;

/*
    Logging Level
    ALL => all levels
    DEBUG => designates fine-grained informational events that are most useful to debug an application
    INFO => informational messages that highlight the progress of the application at the coarse grained level
    WARN => designates potentially harmful situations
    ERROR => designates error events that might still allow the application to continue running
    FATAL => severe error events that presumably lead the application to abort
    OFF => highest possible level, intended to turn off logging
 */

public class Main {
    private Scanner scanner;
    private TicketService ticketService;
    private Repository<Ticket, Integer> ticketRepository;
    private Logger logger = LogManager.getLogger(Main.class.getName());
    private DBConnector connector;


    public static void main(String[] args) {
        Main app = new Main();
        app.run();
    }
    public Main() {
        this.scanner = new Scanner(System.in);
        this.connector = new PostgreSQLDBConnector();
        this.ticketRepository = new TicketSQLRepository(this.connector);
        this.ticketService = new TicketService(this.ticketRepository);
    }
    public void run() {
        List<Ticket> tickets = this.ticketService.getAllTickets();
        for(Ticket t : tickets) {
            System.out.println(t);
        }
//        logger.debug("Starting Ticket Support Application");
//        logger.info("Starting Ticket Support");
//        ((TicketRepository)this.DataAccess.ticketRepository).init(); // make sure the file repository has been properly initialized
//        Ticket t = null;
//
//        System.out.println("What kind of support ticket would you like to submit");
//        System.out.println("1. Tech Support");
//        System.out.println("2. Product Support");
//
//        int ticketTypeSelection = this.scanner.nextInt(); // reading the user input, should be 1 or 2
//        this.scanner.nextLine();
//        t = createTicketFromSelection(ticketTypeSelection); // create a new ticket from the user input;
//
//        // reach here, start collecting information on the ticket
//
//        // submitter
//        System.out.println("Please give user your email address");
//            String submitterEmail = this.scanner.nextLine();
//            t.setSubmitter(submitterEmail);
//
//
//        // description
//        System.out.println("Please describe your issue");
//            String description = this.scanner.nextLine();
//            t.setDescription(description);
//
//
//        // set the submission date
//        t.setSubmittedOn(LocalDateTime.now());
//
//        this.ticketService.saveTicket(t);
//
//        System.out.println("Ticket successfully created");
    }

    private Ticket createTicketFromSelection(int selectionType) {
        return selectionType == 1 ? new TechSupportTicket() : new ProductSupportTicket();
    }
}
