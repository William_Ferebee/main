package com.company.models;

import java.time.LocalDateTime;

public abstract class Ticket {
    private int id;
    private int submitterId;
    private String description;
    private int ticketTypeId;
    private LocalDateTime submittedDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubmitterId() {
        return submitterId;
    }

    public void setSubmitterId(int submitterId) {
        this.submitterId = submitterId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(int ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }

    public LocalDateTime getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(LocalDateTime submittedDate) {
        this.submittedDate = submittedDate;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", submitterId=" + submitterId +
                ", description='" + description + '\'' +
                ", ticketTypeId=" + ticketTypeId +
                ", submittedDate=" + submittedDate +
                '}';
    }
}
