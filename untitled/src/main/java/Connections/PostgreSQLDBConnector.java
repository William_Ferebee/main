package Connections;

import org.postgresql.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgreSQLDBConnector implements DBConnector{
    /*
     * Preload the jdbc driver
     */
    static {
        try {
            DriverManager.registerDriver(new Driver());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new RuntimeException("Could not initialize the jdbc driver", throwables);
        }
    }
    @Override
    public Connection newConnection(String username, String password, String url) throws SQLException{
        return DriverManager.getConnection(url, username, password);
    }

    @Override
    public Connection newConnection() throws SQLException{
        return null;
    }
}
