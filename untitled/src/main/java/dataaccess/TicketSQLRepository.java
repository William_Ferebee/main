package com.company.dataaccess;

import Connections.DBConnector;
import com.company.models.ProductSupportTicket;
import com.company.models.TechSupportTicket;
import com.company.models.Ticket;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TicketSQLRepository implements Repository<Ticket, Integer> {
    private DBConnector connector;

    public TicketSQLRepository(DBConnector connector) {
        this.connector = connector;
    }

    @Override
    public Integer save(Ticket obj) {
        return null;
    }

    @Override
    public Ticket getById(Integer integer) {
        return null;
    }

    @Override
    public void update(Ticket obj) {

    }

    @Override
    public void delete(Ticket obj) {

    }

    @Override
    public Collection<Ticket> getAll() {
        // get a connection database

        try(Connection c = this.connector.newConnection("postgres", "pa$$w0rd123", "jdbc:postgresql://javafs-210907-rds.cvtq9j4axrge.us-east-1.rds.amazonaws.com:5432/postgres")) {
            // define sql statement
            String sql = "SELECT id, submitter_id, description, ticket_type_id, submitted_date from support_tickets";

            // create statement object
            Statement s = c.createStatement();

            // execute sql statement
            ResultSet results = s.executeQuery(sql);
            List<Ticket> tickets = new ArrayList<>();

            // parse over the result
            while(results.next()){
                // create our java objects
                Ticket t = null;
                int ticket_type = results.getInt("ticket_type_id");

                if(ticket_type == 1) {
                    t = new TechSupportTicket();
                } else {
                    t = new ProductSupportTicket();
                }

                t.setSubmitterId(results.getInt("submitter_id"));
                t.setId(results.getInt("id"));
                t.setDescription(results.getString("description"));
                t.setTicketTypeId(ticket_type);
                t.setSubmittedDate(results.getObject("submitted_date", LocalDateTime.class));

                tickets.add(t);
            }
            return tickets;

        } catch(Exception ex) {
            throw new RuntimeException("Could not fetch tickets", ex);
        }
    }
}
