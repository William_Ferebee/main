package com.company.dataaccess;

import java.util.Collection;

public interface Repository<T, ID>{
    ID save(T obj);
    T getById(ID id);
    void update(T obj);
    void delete(T obj);
    Collection<T> getAll();
}
