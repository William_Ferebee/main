package HibernateManagers;

import javax.persistence.*;

import DAO.LoginController;
import lombok.*;

@ToString
@Setter
@Getter

@Entity
@Table(name = "managers")

public class Managers  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")

    private Long id;
    private String name;
    private String email;
    private String password;

}
