/**
 * Project 1  Futures, Monies, Lending Bank (Reimbursement)
 * William Ferebee
 */
import DAO.ApprovalController;
import DAO.LoginController;
import DAO.RepositoryController;
import HibernateEmployees.Employees;
import HibernateManagers.Managers;
import io.javalin.http.Handler;
import io.javalin.http.staticfiles.Location;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import io.javalin.Javalin;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import io.javalin.core.JavalinConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.logging.*;
import java.io.Serializable;
import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.ge;



public class Main {
    private Logger logger = LogManager.getLogger(Main.class);
    private static Object LoginController;
    private SessionFactory sessionFactory;


    public static void main(String[] args) {

        Javalin app = Javalin.create(config -> config.addStaticFiles
                ("WebPageFiles/FMLHomePage.html", Location.CLASSPATH)).start(7000);

        Main superMain = new Main();
        superMain.configure();
        superMain.initEntities();

        app.get("/api/employees", (Handler) LoginController);
    }

    private void initEntities() {
        Employees employees = new Employees();

        Managers managers = new Managers();
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(employees);
        session.save(managers);
        tx.commit();
    }
    private void configure() {
        Configuration configuration = new Configuration().configure();
        if (configuration != null) {
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                this.sessionFactory = configuration.buildSessionFactory(builder.build());
            }
    }
    }








