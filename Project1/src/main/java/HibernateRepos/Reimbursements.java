package HibernateRepos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "reimbursements")

public class Reimbursements {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    private String name;
    private String reimbursementRq;
    private Timestamp submitTime;
    private String approval;
    private String reason;
    private double amount;
    private Timestamp approveTime;



    @Override
    public String toString() {
        return "Reimbursements{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", reimbursementRq='" + reimbursementRq + '\'' +
                ", submitTime=" + submitTime +
                ", approval='" + approval + '\'' +
                ", reason='" + reason + '\'' +
                ", amount=" + amount +
                ", approveTime=" + approveTime +
                '}';
    }
}
