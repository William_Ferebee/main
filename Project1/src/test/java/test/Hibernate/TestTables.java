package test.Hibernate;

import HibernateEmployees.Employees;
import HibernateManagers.Managers;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Test;


public class TestTables {

    private SessionFactory sessionFactory;
    @Test
    public static void main(String[] args) {



        TestTables tableTest = new TestTables();
        tableTest.configure();

        Employees e = tableTest.getSingleEmployeeByID(1);
        System.out.println(e);

        Managers m = tableTest.getSingleManagerByID(1);
        System.out.println(m);
    }

    private void configure() {
        Configuration configuration = new Configuration().configure();

        if (configuration != null) {
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            this.sessionFactory = configuration.buildSessionFactory(builder.build());
        }
    }

    private Employees getSingleEmployeeByID(long id) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Employees employee = (Employees) session.get(Employees.class, id);
        tx.commit();
        Assert.assertNotNull(employee);
        System.out.println("Test passed\nGrabbed data from employees table");
        return employee;
    }

    private Managers getSingleManagerByID(long id) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Managers manager = (Managers) session.get(Managers.class, id);
        tx.commit();
        Assert.assertNotNull(manager);
        System.out.println("Test passed\nGrabbed data from managers table");
        return manager;
    }
}



